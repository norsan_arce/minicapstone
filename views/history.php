<?php
require "../partials/template.php";

function get_title(){
echo "History Page";
}

function get_body_contents(){
require "../controllers/connection.php";
?>
<h1 class="text-center py-5">History Page</h1>
<div class="col-lg-8 offset-lg-2">
	<table class="table table-striped">
		<thead>
			<th>Id</th>
			<th>Image</th>
			<th>Item</th>
			<th>Total</th>
		</thead>
	<tbody>
<?php
$history_query = "SELECT item_order.id,items.imgPath,items.name,orders.total FROM item_order JOIN items ON (items.id = item_order.item_id)JOIN orders ON (orders.id = item_order.order_id) WHERE orders.user_id = 4";
$history = mysqli_query($conn, $history_query);

foreach ($history as $indiv_history) {
?>
<tr>
	<td><?= $indiv_history['id']?></td>
	<td><img src="<?= $indiv_history['imgPath']?>" height="100px"></td>
	<td><?= $indiv_history['name']?></td>
	<td><?= $indiv_history['total']?></td>
</tr>
<?php
}
?>
	</tbody>
	</table>
</div>
<?php
}

?>