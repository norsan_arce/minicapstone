<?php 
require "../partials/template.php";

function get_title(){
	echo "Profile";
}

function get_body_contents(){
	require "../controllers/connection.php";
	?>
	<h1 class="text-center py-2">Profile Page</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
				<div class="card">
					<img class="card-img-top" src="../assets/images/admin.png" height="250px" width="250px">
					<div class="card-body">
						<h5 class="card-title">Details:</h5>
						<h6 class="card-text">First Name: <?php echo  $_SESSION['user']['firstName'] ?></h6>
						<h6 class="card-text">Last Name: <?php echo  $_SESSION['user']['lastName'] ?></h6>
						<h6 class="card-text">E-mail: <?php echo  $_SESSION['user']['email'] ?></h6>
					</div>
				</div>
			</div>
			<div class="col-lg-7">
				<h3>Addresses:</h3>
				<ul>
					<?php 
					$userId = $_SESSION['user']['id'];
					$address_query = "SELECT * FROM addresses WHERE user_id = '$userId'";
					$addresses = mysqli_query($conn, $address_query);
					foreach($addresses as $indiv_address){
						?>
						<li>
							<?php echo $indiv_address['address1'] . ", " . $indiv_address['address2'] .
							"<br>" . $indiv_address['city'] . "<br>" . $indiv_address['zipCode']?>
						</li>
						<?php
					}
					?>
				</ul>
				<form action="../controllers/add-address-process.php" method="POST">
					<div class="form-group">
						<label for="address1">Address 1:</label>
						<input type="text" name="address1" class="form-control">
					</div>
					<div class="form-group">
						<label for="address2">Address 2:</label>
						<input type="text" name="address2" class="form-control">
					</div>
					<div class="form-group">
						<label for="city">City:</label>
						<input type="text" name="city" class="form-control">
					</div>
					<div class="form-group">
						<label for="zipCode">Zip Code:</label>
						<input type="text" name="zipCode" class="form-control">
					</div>
					<input type="hidden" name="user_id" value="<?php echo $userId?>">
					<button class="btn btn-secondary" type="submit">Add Address</button>
				</form>
			</div>
			<div class="col-lg-7 offset-lg-5">
				<h3>Contacts:</h3>
				<ul>
					<?php 
					$contacts_query = "SELECT * FROM contacts WHERE user_id = '$userId'";
					$contacts = mysqli_query($conn, $contacts_query);
					foreach($contacts as $indiv_contact){
						?>
						<li>
							<?php echo $indiv_contact['contactNo'] . "<br>" ?>
						</li>
						<?php
					}
					?>
				</ul>
				<form action="../controllers/add-contacts.php" method="POST">
					<div class="form-group">
						<label for="address1">Contact #:</label>
						<input type="text" name="contactNo" class="form-control">
					</div>
					<input type="hidden" name="user_id" value="<?php echo $userId?>">
					<button class="btn btn-secondary" type="submit">Add Contacts</button>
				</form>
			</div>
		</div>
	</div>

	<?php 
}
?>
